FLYING SAUCER XML/CSS RENDERER LICENSE COMMENTS
Copyright (C) 2015 Patrick Wright 
https://code.google.com/p/flying-saucer/

All source code to Flying Saucer itself is licensed under the GNU Lesser General
Public License (LGPL); you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 2.1 of the License, or (at your option) any later version.
A copy of the LGPL can be found
on the website of the Free Software Foundation, at 
http://www.gnu.org/copyleft/lesser.html, and in our distributions under
LICENSE-LGPL-2.1.txt or LICENSE-LGPL-3.txt.

Flying Saucer relies on several other free or open source projects in 
order to build and run. Where binary Java JAR files are included, we include
only the unmodified binary releases as provided by those other projects. 
Source code for the respective projects can be found on the project 
websites, listed below.

Java projects that are used for building and running Flying Saucer are:


JUnit (for testing)
http://junit.org
License: Common Public License Version 1.0
Using version 3.8.1
Included as lib/junit.jar

Ant (for building)
http://ant.apache.org/
License: Apache Software License Version 2.0
Not packaged with release; development using version 1.6.x

iText (PDF generation)
http://itextpdf.com/
License: Mozilla Public License Version 1.1
Using version 2.1.7.
Included as lib/iText-2.1.7.jar

iText 5 (PDF generation only used in flying-saucer-pdf-itext5)
http://itextpdf.com/
License: Affero General Public License
Using version 5.3.0.

SVGSalamander (SVG rendering in demo)
https://svgsalamander.java.net/
License: LGPL
Using version 1, released on the project website

SWT (Standard Widget Toolkit)
http://www.eclipse.org/swt/
License: Eclipse Public License Version 1.0 ("EPL"). A copy of the EPL is available at http://www.eclipse.org/legal/epl-v10.html
Including version 3.5 libraries for Windows, Mac, and Linux

DocBook CSS (DocBook XML Rendering with CSS)
  MozBook CSS (public domain, released by David Horton)
  WSIWYGDocBook 1.01 -- see demos\docbook\wysiwygdocbook1.01, and COPYING therein
  docbook-css-0.4 -- see demos\docbook\docbook-css-0.4, and COPYING therein

W3C CSS Test Suite
  Distributed with our source bundle for the convenience of our developers.
  License is W3C Document License, see LICENSE_W3C_TEST.
  Source is http://www.w3.org/Style/CSS/Test/, for the most current version
  please see that URL.

Special thanks to Andy Streich, et. al. for Xilize
Xilize Text to HTML library
http://xilize.sourceforge.net/
License: GPL
Used version 3.x, only to produce documentation
Shipped with our source bundle as a convenience for developers rebuilding 
documentation; we do not use nor link to the Xilize libraries at runtime

BeanShell
http://www.beanshell.org
License: LGPL (dual-license with SPL)
Use version 2.x as Xilize has a dependency on it; used only to produce 
documentation.

XML-APIs (extracted from Apache Xerces-2)
http://xerces.apache.org/xerces2-j/
License: Apache v2
We include the xml-apis.jar from the Xerces binary distribution in order to allow
our code to compile on JDK 1.4, which does not include newer XML APIs, even though
these API implementations will run on version 1.4 of the JRE. The JAR is unmodified
from the Xerces release, but is renamed as xml-apis-xerces-2.9.1.jar to
make the version clear.
Included as lib/xml-apis-xerces-2.9.1.jar
Digitized data copyright (c) 2010 Google Corporation
	with Reserved Font Arimo, Tinos and Cousine.
Copyright (c) 2012 Red Hat, Inc.
	with Reserved Font Name Liberation.

This Font Software is licensed under the SIL Open Font License,
Version 1.1.

This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL

SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007

PREAMBLE The goals of the Open Font License (OFL) are to stimulate
worldwide development of collaborative font projects, to support the font
creation efforts of academic and linguistic communities, and to provide
a free and open framework in which fonts may be shared and improved in
partnership with others.

The OFL allows the licensed fonts to be used, studied, modified and
redistributed freely as long as they are not sold by themselves.
The fonts, including any derivative works, can be bundled, embedded,
redistributed and/or sold with any software provided that any reserved
names are not used by derivative works.  The fonts and derivatives,
however, cannot be released under any other type of license.  The
requirement for fonts to remain under this license does not apply to
any document created using the fonts or their derivatives.

 

DEFINITIONS
"Font Software" refers to the set of files released by the Copyright
Holder(s) under this license and clearly marked as such.
This may include source files, build scripts and documentation.

"Reserved Font Name" refers to any names specified as such after the
copyright statement(s).

"Original Version" refers to the collection of Font Software components
as distributed by the Copyright Holder(s).

"Modified Version" refers to any derivative made by adding to, deleting,
or substituting ? in part or in whole ?
any of the components of the Original Version, by changing formats or
by porting the Font Software to a new environment.

"Author" refers to any designer, engineer, programmer, technical writer
or other person who contributed to the Font Software.


PERMISSION & CONDITIONS

Permission is hereby granted, free of charge, to any person obtaining a
copy of the Font Software, to use, study, copy, merge, embed, modify,
redistribute, and sell modified and unmodified copies of the Font
Software, subject to the following conditions:

1) Neither the Font Software nor any of its individual components,in
   Original or Modified Versions, may be sold by itself.

2) Original or Modified Versions of the Font Software may be bundled,
   redistributed and/or sold with any software, provided that each copy
   contains the above copyright notice and this license. These can be
   included either as stand-alone text files, human-readable headers or
   in the appropriate machine-readable metadata fields within text or
   binary files as long as those fields can be easily viewed by the user.

3) No Modified Version of the Font Software may use the Reserved Font
   Name(s) unless explicit written permission is granted by the
   corresponding Copyright Holder. This restriction only applies to the
   primary font name as presented to the users.

4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
   Software shall not be used to promote, endorse or advertise any
   Modified Version, except to acknowledge the contribution(s) of the
   Copyright Holder(s) and the Author(s) or with their explicit written
   permission.

5) The Font Software, modified or unmodified, in part or in whole, must
   be distributed entirely under this license, and must not be distributed
   under any other license. The requirement for fonts to remain under
   this license does not apply to any document created using the Font
   Software.


 
TERMINATION
This license becomes null and void if any of the above conditions are not met.

 

DISCLAIMER
THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER
DEALINGS IN THE FONT SOFTWARE.
