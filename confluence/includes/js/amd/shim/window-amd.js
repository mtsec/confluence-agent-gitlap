/**
 * @module window
 */
define("window", function () {
    "use strict";
    return window;
});