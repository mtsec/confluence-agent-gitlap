/**
 * @module aui/flag
 */
define("aui/flag", ['aui/flag'], function (Flag) {
    "use strict";
    return Flag;
});