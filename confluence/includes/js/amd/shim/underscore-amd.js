/**
 * @module underscore
 */
define('underscore', function () {
    "use strict";
    return _;
});