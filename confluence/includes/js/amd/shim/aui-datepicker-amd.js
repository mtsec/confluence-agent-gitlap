/**
 * @module aui/datepicker
 */
define("aui/datepicker", function () {
    "use strict";
    return AJS.DatePicker;
});
