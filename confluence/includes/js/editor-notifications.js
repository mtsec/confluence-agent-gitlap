define('confluence/editor-notifications', [
], function(
) {
    'use strict';

    var notificationFlag;

    /**
     * @exports confluence/editor-notifications
     */
    var editorNotifications = {
        /**
         * Display a notification.
         *
         * @param {string} type the type of message. Can be one of error, warning, info or success.
         * @param {string} message the message that will be notified (without any encoding so ensure the message is HTML safe).
         * @param {string} [close] aui Flag close type. If not provided, if type is error, close will be manual; otherwise close will be auto.
         * @method notify
         */
        notify: function(type, message, close) {
            var Flag = require('aui/flag');
            var closeType = close;

            // CONFDEV-31504 auto-dismiss existing editor flag before showing a new one
            if (notificationFlag !== undefined && notificationFlag.getAttribute('aria-hidden') !== 'true') {
                notificationFlag.close();
            }

            if (closeType === undefined) {
                // persistent errors, transient warnings
                closeType = type === 'error' ? 'manual' : 'auto';
            }

            notificationFlag = Flag({
                type: type,
                body: message,
                close: closeType
            });
        }
    };
    return editorNotifications;
});

require('confluence/module-exporter').safeRequire('confluence/editor-notifications', function(EditorNotification) {
    'use strict';

    var event = require('confluence/api/event');
    var Confluence = require('confluence/legacy');

    event.bind('init.rte', function() {
        Confluence.EditorNotification = EditorNotification;
    });
});
