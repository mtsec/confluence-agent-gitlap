<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ page import="com.atlassian.confluence.impl.pages.attachments.filesystem.AttachmentDataFileSystemInternals,
                 com.atlassian.confluence.spaces.Space,
                 com.atlassian.confluence.spaces.SpaceManager,
                 com.atlassian.confluence.util.HtmlUtil,
                 com.atlassian.spring.container.ContainerManager" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.io.File,
                 java.util.Optional" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Find Space Attachment Storage Location</title>
</head>
<body>
<%
    if ("Find".equals(request.getParameter("action")))
    {
        String spaceKey = request.getParameter("spacekey");
        if (StringUtils.isBlank(spaceKey))
        {
%>
<p>
<strong>The space key parameter must be supplied</strong>
</p>
<%
        }
        else
        {
                SpaceManager spaceManager = (SpaceManager) ContainerManager.getComponent("spaceManager");
                Space space = spaceManager.getSpace(spaceKey);
                if (space == null)
                {
%>
<p>
<strong>No space was found for the key <%= HtmlUtil.htmlEncode(spaceKey) %></strong>
</p>
<%
                }
                else
                {
                    AttachmentDataFileSystemInternals attachmentDataFileSystem = (AttachmentDataFileSystemInternals) ContainerManager.getComponent("localAttachmentDataFileSystem");
                    File spaceDir = attachmentDataFileSystem.getDirectoryForSpace(Optional.of(space.getId()));
                    if (spaceDir != null)
                    {
%>
<p>
    Attachments for the space <strong><%= HtmlUtil.htmlEncode(space.getName()) %></strong> (key=<strong><%= HtmlUtil.htmlEncode(spaceKey) %></strong>) are stored at -
</p>
<ul>
    <li name="spaceAttachmentLocation"><%= HtmlUtil.htmlEncode(spaceDir.getAbsolutePath()) %>
</ul>
<hr/>
                    }
                    else
                    {%>
<p>Your attachment configuration is not supported by this tool.</p>
<%
                    }
                }
        }
    }
%>
<p>
Given a valid space key, this page will provide you with the location on the file system where the attachments for this space are stored.
</p>
<p>
If this Confluence instance is not using file system attachment storage then this will be indicated.
</p>
<form name="spacekeyform" id="spacekeyform"
    action="findspaceattachments.jsp" method="post">
    <input type="text" size="10" name="spacekey"/>
    <input type="submit" name="action" value="Find" />
</form>
</body>
</html>
