# This is the logging configuration for Confluence. This is standard log4j.configuration as
# described at http://logging.apache.org/log4j/1.2/manual.html
#
# To turn more verbose logging on - change "ERROR" to "WARN" or "DEBUG"

log4j.rootLogger=WARN, confluencelog, errorlog, fluentdAppender
#  ,  specialvelocitylog

###
# LOGGING LOCATION AND APPENDER
#
# Here we define the default appender which after bootstrap will log to logs/atlassian-confluence.log
# within your configured confluence.home. Prior to that, logging will be to the console.
#
# Use the ConsoleAppender for development to leave the logging configuration up to the app server/IDEA.
#
# If you want to log to a different location uncomment the RollingFileAppender line and the File setting
# as instructed below.
###
log4j.appender.confluencelog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
# Comment the above line and uncomment the following 2 lines if you want to log to a specific location
#log4j.appender.confluencelog=org.apache.log4j.RollingFileAppender
#log4j.appender.confluencelog.File=${catalina.home}/logs/atlassian-confluence.log
log4j.appender.confluencelog.LogFileName=atlassian-confluence.log
log4j.appender.confluencelog.Threshold=DEBUG
log4j.appender.confluencelog.MaxFileSize=20480KB
log4j.appender.confluencelog.MaxBackupIndex=5
log4j.appender.confluencelog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.confluencelog.layout.ConversionPattern=%d %p [%t] [%c{4}] %M %m%n

log4j.appender.synchronylog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
# Comment the above line and uncomment the following 2 lines if you want to log to a specific location
#log4j.appender.synchronylog=org.apache.log4j.RollingFileAppender
#log4j.appender.synchronylog.File=${catalina.home}/logs/atlassian-synchrony.log
log4j.appender.synchronylog.LogFileName=atlassian-synchrony.log
log4j.appender.synchronylog.Threshold=DEBUG
log4j.appender.synchronylog.MaxFileSize=20480KB
log4j.appender.synchronylog.MaxBackupIndex=5
log4j.appender.synchronylog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.synchronylog.layout.ConversionPattern=%d %p [%t] %m%n

#log4j.appender.luceneQuery=org.apache.log4j.ConsoleAppender
#log4j.appender.luceneQuery.Threshold=DEBUG
#log4j.appender.luceneQuery.layout=com.atlassian.confluence.util.PatternLayoutWithContext
#log4j.appender.luceneQuery.layout.ConversionPattern=%m%n

#log4j.appender.specialvelocitylog=org.apache.log4j.FileAppender
#log4j.appender.specialvelocitylog.bufferedIO=true
#log4j.appender.specialvelocitylog.File=velocity-webapp-output.log
#log4j.appender.specialvelocitylog.layout=org.apache.log4j.PatternLayout
#log4j.appender.specialvelocitylog.layout.ConversionPattern=%m%n

#log4j.logger.VELOCITY=DEBUG, specialvelocitylog
#log4j.additivity.VELOCITY=false

log4j.appender.securitylog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.securitylog.LogFileName=atlassian-confluence-security.log
log4j.appender.securitylog.MaxFileSize=20480KB
log4j.appender.securitylog.MaxBackupIndex=5
log4j.appender.securitylog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.securitylog.layout.ConversionPattern=%d %p [%t] [%c{4}] %M %m%n
# All logs from the below packages will go to atlassian-confluence-security.log.
# If you want to write those logs to atlassian-confluence.log or any other root loggers,
# then set log4j.additivity into true (for e.g. log4j.additivity.com.atlassian.user=true). Each package should be set individually.
log4j.logger.com.atlassian.user=WARN, securitylog
log4j.additivity.com.atlassian.user=false
log4j.logger.com.atlassian.confluence.user=WARN, securitylog
log4j.additivity.com.atlassian.confluence.user=false
log4j.logger.com.atlassian.seraph=WARN, securitylog
log4j.additivity.com.atlassian.seraph=false
log4j.logger.com.atlassian.crowd=WARN, securitylog
log4j.additivity.com.atlassian.crowd=false
log4j.logger.com.atlassian.confluence.security=WARN, securitylog
log4j.additivity.com.atlassian.confluence.security=false
# We need to override rule for this package because Fast (denormalised) permissions logs should go to the main log only
log4j.logger.com.atlassian.confluence.security.denormalisedpermissions=WARN, confluencelog
log4j.additivity.com.atlassian.confluence.security.denormalisedpermissions=false
log4j.logger.org.springframework.ldap.core=WARN, securitylog
log4j.additivity.org.springframework.ldap.core=false

log4j.appender.indexlog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.indexlog.LogFileName=atlassian-confluence-index.log
log4j.appender.indexlog.MaxFileSize=20480KB
log4j.appender.indexlog.MaxBackupIndex=5
log4j.appender.indexlog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.indexlog.layout.ConversionPattern=%d %p [%t] [%c{4}] %M %m%n
# All logs from these packages will go to atlassian-confluence-index.log.
# If you want to write those logs to atlassian-confluence.log or any other root loggers,
# then set log4j.additivity into true (for e.g. log4j.com.atlassian.confluence.internal.index=true). Each package should be set individually.
log4j.logger.com.atlassian.confluence.internal.index=INFO, indexlog
log4j.additivity.com.atlassian.confluence.internal.index=false
log4j.logger.com.atlassian.confluence.search.lucene=WARN, indexlog
log4j.additivity.com.atlassian.confluence.search.lucene=false
log4j.logger.com.atlassian.bonnie.search.extractor=WARN, indexlog
log4j.additivity.com.atlassian.bonnie.search.extractor=false

log4j.appender.outgoingmaillog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.outgoingmaillog.LogFileName=atlassian-confluence-outgoing-mail.log
log4j.appender.outgoingmaillog.MaxFileSize=20480KB
log4j.appender.outgoingmaillog.MaxBackupIndex=5
log4j.appender.outgoingmaillog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.outgoingmaillog.layout.ConversionPattern=%d %p [%t] [%c{4}] %M %m%n
# All logs from the below packages will go to atlassian-confluence-outgoing-mail.log.
# If you want to write those logs to atlassian-confluence.log or any other root loggers,
# then set log4j.additivity into true (for e.g. log4j.com.atlassian.confluence.mail=true). Each package should be set individually.
log4j.logger.com.atlassian.confluence.mail=WARN, outgoingmaillog
log4j.additivity.com.atlassian.confluence.mail=false
log4j.logger.com.atlassian.confluence.jmx=WARN, outgoingmaillog
log4j.additivity.com.atlassian.confluence.jmx=false
log4j.logger.com.atlassian.plugin.notifications.dispatcher=WARN, outgoingmaillog
log4j.additivity.com.atlassian.plugin.notifications.dispatcher=false
log4j.logger.com.atlassian.confluence.plugins.email=WARN, outgoingmaillog
log4j.additivity.com.atlassian.confluence.plugins.email=false
log4j.logger.javax.mail=WARN, outgoingmaillog
log4j.additivity.javax.mail=false
log4j.logger.com.sun.mail=WARN, outgoingmaillog
log4j.additivity.com.sun.mail=false
log4j.logger.com.atlassian.mail = WARN, outgoingmaillog
log4j.additivity.com.atlassian.mail = false

log4j.appender.sqllog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.sqllog.LogFileName=atlassian-confluence-sql.log
log4j.appender.sqllog.MaxFileSize=20480KB
log4j.appender.sqllog.MaxBackupIndex=5
log4j.appender.sqllog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.sqllog.layout.ConversionPattern=%d %p [%t] [%c{4}] %M %m%n
# All logs from the below packages will go to atlassian-confluence-sql.log.
# If you want to write those logs to atlassian-confluence.log or any other root loggers,
# then set log4j.additivity into true (for e.g. log4j.additivity.org.hibernate.SQL=true). Each package should be set individually.
#
# Log hibernate prepared statements/SQL queries (equivalent to setting 'hibernate.show_sql' to 'true').
# DEBUG, TRACE or ALL is required to see query strings.
log4j.logger.org.hibernate.SQL=ERROR, sqllog
log4j.additivity.org.hibernate.SQL=false
log4j.logger.org.hibernate.type.descriptor.sql=WARN, sqllog
log4j.additivity.org.hibernate.type.descriptor.sql=false

# This appender will post logs events in JSON format to a fluentd service.
# It is inactive by default, and activation requires setting the "atlassian.logging.cloud.enabled" system property to "true"
log4j.appender.fluentdAppender=com.atlassian.logging.log4j.appender.FluentdAppender
# This is currently hard-wired, since FluentdAppender currently doesn't allow it to be configured via system property.
log4j.appender.fluentdAppender.FluentdEndpoint=http://localhost:9880/
# NOTE: DO NOT SET THE THRESHOLD TO INCLUDE DEBUG! The appender itself spits out debug logs, so this would cause an
# infinite (linearly growing) loop of logging itself.
log4j.appender.fluentdAppender.Threshold=INFO
log4j.appender.fluentdAppender.layout=com.atlassian.logging.log4j.layout.JsonLayout
log4j.appender.fluentdAppender.layout.FilteringApplied=true
log4j.appender.fluentdAppender.layout.MinimumLines=6
log4j.appender.fluentdAppender.layout.ShowEludedSummary=false
log4j.appender.fluentdAppender.layout.FilteredFrames=@confluence-filtered-frames.properties

####################################
# LOGGING LEVELS
####################################

#log4j.logger.com.atlassian.confluence.core=DEBUG

#log4j.logger.com.atlassian.confluence.search=DEBUG

###
# Atlassian User
###
#log4j.logger.com.atlassian.user=DEBUG
#log4j.logger.com.atlassian.confluence.user=DEBUG
#log4j.logger.bucket.user=DEBUG
#log4j.logger.com.atlassian.seraph=DEBUG
#log4j.logger.com.opensymphony.user=DEBUG

###
# Attachment Migration
###
#log4j.logger.com.atlassian.confluence.pages.persistence.dao=DEBUG

###
# INFO logging for startup event
###
log4j.logger.com.atlassian.confluence.lifecycle=INFO

log4j.logger.com.atlassian.confluence.status.SystemErrorInformationLogger=INFO
###
# INFO logging for upgrade tasks.
###
log4j.logger.com.atlassian.confluence.internal.upgrade=INFO
log4j.logger.com.atlassian.confluence.upgrade=INFO

# Cluster events
log4j.logger.com.atlassian.confluence.cluster=INFO

# Cluster safety logging
log4j.logger.com.atlassian.confluence.cluster.safety=INFO

log4j.logger.com.atlassian.confluence.event.listeners.ResetHiLoAfterImportListener=INFO

log4j.logger.com.atlassian.confluence.admin.actions=INFO

# Confluence auditing
log4j.logger.com.atlassian.confluence.api.impl.service.audit=INFO

# INFO logging for RPC actions
log4j.logger.com.atlassian.confluence.rpc.auth.TokenAuthenticationInvocationHandler=INFO

#log4j.logger.com.mchange.v2.resourcepool.BasicResourcePool=DEBUG

# Dont display web dependency resolution warnings, which can't be fixed on production
log4j.logger.com.atlassian.plugin.webresource.DefaultResourceDependencyResolver=ERROR

###
# THREAD LOCAL ERROR LOG APPENDER
###
log4j.appender.errorlog=com.atlassian.core.logging.ThreadLocalErrorLogAppender
log4j.appender.errorlog.Threshold=WARN

# We have too problems in Velocity at the moment to show errors by default. Fatal in Prod, Error in Dev
log4j.logger.velocity=FATAL

## mail jobs debugging
#log4j.logger.com.atlassian.confluence.mail.jobs=DEBUG

## lucene debugging
#log4j.logger.com.atlassian.confluence.search.lucene=DEBUG
#log4j.logger.com.atlassian.bonnie=DEBUG

# suppress WARN log statements from the CoherenceCacheStrategy class (CONF-2517 & CONFDEV-22887)
log4j.logger.com.atlassian.hibernate.extras.tangosol.CoherenceCacheStrategy=ERROR

# View SpacePermission cache hits and misses
#log4j.logger.com.atlassian.confluence.security=DEBUG

# Tangosol Coherence logging -- see also severity in tangosol-coherence-override.xml.
#log4j.logger.Coherence=DEBUG

#####################
# Hibernate logging #
#####################

## log hibernate prepared statement parameter values. Note: due to caching in net.sf.hibernate.type.NullableType, requires restart to take effect
## TRACE or ALL is required to see parameter values
## Note that log4j.appender.confluencelog.Threshold (or other appenders) also must be TRACE or ALL to see any trace messages in the logs
#log4j.logger.net.sf.hibernate.type=TRACE

# suppress JDBCExceptionReporter warnings (there can be a few of these under DB2)
log4j.logger.net.sf.hibernate.util.JDBCExceptionReporter=ERROR

# suppress warnings from the SessionImpl (like Narrowing proxy to class com.atlassian.confluence.pages.Page - this operation breaks ==)
log4j.logger.net.sf.hibernate.impl.SessionImpl=ERROR

# Deprecation warnings from Hibernate is never something we want to see in production instances.
log4j.logger.org.hibernate.orm.deprecation=ERROR

# Log high-level import/export information
#log4j.logger.com.atlassian.confluence.importexport=INFO

# Log when imports begin and end to aid the understanding of memory spikes during monitoring.
log4j.logger.com.atlassian.confluence.importexport.actions.ImportLongRunningTask=INFO

# suppress PDF export logs to errors only
log4j.logger.com.atlassian.confluence.importexport.impl.PdfExporter=ERROR

log4j.logger.org.apache.fop=ERROR

# suppress FileUtils (file management) warnings
log4j.logger.com.atlassian.core.util.FileUtils=ERROR

# suppress hibernate CustomType warnings
# (BlobInputStreamType is not a serializable custom type)
log4j.logger.net.sf.hibernate.type.CustomType=ERROR

# suppress WARN log statements from the Hibernate Ehcache classes
log4j.logger.org.hibernate.cache.ehcache.internal.strategy.AbstractReadWriteEhcacheAccessStrategy=ERROR
log4j.logger.org.hibernate.cache.ehcache.AbstractEhcacheRegionFactory=ERROR
log4j.logger.org.hibernate.cache.ehcache.EhCacheRegionFactory=ERROR

#log4j.logger.bucket.search=DEBUG
#log4j.logger.org.apache.lucene.search=DEBUG
#log4j.logger.com.atlassian.confluence.search=DEBUG
#log4j.logger.com.atlassian.confluence.search.summary=INFO
#log4j.logger.com.atlassian.confluence.search.UnindexingHibernateInterceptor=DEBUG

### useful confluence classes
#log4j.logger.com.atlassian.confluence=INFO
#log4j.logger.com.atlassian.confluence.setup=DEBUG
#log4j.logger.com.atlassian.confluence.setup.actions=DEBUG
#log4j.logger.com.atlassian.confluence.util=DEBUG

### show progress building of ANCESTOR table
#log4j.logger.com.atlassian.confluence.pages.ancestors.HibernatePageAncestorManager=INFO

### hibernate
#log4j.logger.net.sf.hibernate=DEBUG

### log JDBC bind parameters ###
# log4j.logger.net.sf.hibernate.type=DEBUG

### hibernate caching activity
#log4j.logger.net.sf.hibernate.cache=DEBUG

### log prepared statement cache activity ###
#log4j.logger.net.sf.hibernate.ps.PreparedStatementCache=DEBUG

### opensymphony (sitemesh, webwork, xwork)
#log4j.logger.com.opensymphony=DEBUG

### Spring
# log4j.logger.org.springframework=WARN
# log4j.logger.springframework.transaction.support.TransactionSynchronizationManager=DEBUG
# log4j.logger.com.atlassian.config.FlushingSpringSessionInViewFilterForHibernate=DEBUG

# Suppress "Found more than one MBeanServer" warnings
log4j.logger.org.springframework.jmx.support.JmxUtils=ERROR

### Apache stuff
# log4j.logger.org.apache=WARN

# Suppress "Cookie rejected" messages from HttpClient
log4j.logger.org.apache.commons.httpclient.HttpMethodBase=ERROR

### WIKI Engine
#log4j.logger.com.atlassian.renderer=WARN

### Plugin subsystem
#log4j.logger.org.springframework.osgi=ALL
#log4j.logger.com.atlassian.plugin.osgi=DEBUG
#log4j.logger.com.atlassian.plugin.DefaultPluginManager=DEBUG

# Plugins can use "atlassian.plugin" loggers across all products (Platform 2.7 requirement)
log4j.logger.atlassian.plugin=INFO

## Log smartlist queries
#log4j.logger.com.atlassian.confluence.core.LuceneSmartListManager=DEBUG

## PROFILING LOG APPENDER
log4j.appender.profilerlog=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.profilerlog.Threshold=DEBUG
log4j.appender.profilerlog.LogFileName=atlassian-confluence-profiler.log
log4j.appender.profilerlog.MaxFileSize=20480KB
log4j.appender.profilerlog.MaxBackupIndex=20
log4j.appender.profilerlog.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.profilerlog.layout.ConversionPattern=%d [%t] %n%m%n
log4j.logger.com.atlassian.util.profiling=DEBUG, profilerlog
log4j.additivity.com.atlassian.util.profiling=false

## profiling debugging
#log4j.logger.com.atlassian.confluence.util=DEBUG, profilerlog
#log4j.logger.com.atlassian.confluence.impl.hibernate.ConfluenceHibernateTransactionManager=DEBUG, profilerlog
#log4j.logger.com.opensymphony.oscache.web.filter=DEBUG, profilerlog
#log4j.logger.net.sf.hibernate.ps.PreparedStatementCache=DEBUG, profilerlog
#log4j.logger.org.hibernate.type=DEBUG, profilerlog

####
# Access log configuration
####
# Uncomment the lines below and the access log filter-mapping in web.xml to enable the logging
# You will probably want to create a new appender so that access log messages go to a different file.
#log4j.category.com.atlassian.confluence.util.AccessLogFilter=INFO

## caching debugging
#log4j.logger.com.atlassian.confluence.cache=DEBUG

## indexing debugging
#log4j.logger.com.atlassian.confluence.search.lucene=DEBUG

# Index snapshots management
log4j.logger.com.atlassian.confluence.internal.index.lucene.snapshot=INFO

# System maintenance task runners
log4j.logger.com.atlassian.confluence.impl.system.runner=INFO

# Debugging to troubleshoot duplicate key in BANDANA table problem
#log4j.logger.com.atlassian.hibernate.ResettableTableHiLoGenerator=DEBUG

#log4j.logger.com.atlassian.confluence.search.didyoumean.lucene.LuceneWordSuggester=DEBUG

# Default log level is WARN. If you want to log exceptions as well if an action is not found
# set the level to DEBUG
log4j.logger.com.atlassian.confluence.servlet.ConfluenceServletDispatcher=WARN

log4j.logger.com.atlassian.confluence.search.v2.lucene.SearcherAction=WARN
# , luceneQuery
#log4j.additivity.com.atlassian.confluence.search.v2.lucene.SearcherAction=false

# reduce the number of logs generated by the AbstractNoOpServlet
# http://jira.atlassian.com/browse/CONF-13496
log4j.logger.com.atlassian.core.servlet.AbstractNoOpServlet=ERROR

# Hide warnings logged by Shindig at startup (CONF-20692)
log4j.logger.org.apache.shindig.gadgets=ERROR

# We have some classes that don't have interfaces. We don't need to see a warning about this on every startup
log4j.logger.com.atlassian.plugin.osgi.hostcomponents.impl.DefaultComponentRegistrar=ERROR

# Provides visibility on what is happening on startup (notably when and how long the spring and plugin systems take to load)
log4j.logger.com.atlassian.plugin.manager.DefaultPluginManager=INFO
log4j.logger.org.springframework.web.context.ContextLoader=INFO

# CONF-17809
log4j.logger.org.apache.http.impl.client.DefaultRequestDirector=ERROR

# Embedded Crowd logging
log4j.logger.com.atlassian.crowd.embedded=INFO
log4j.logger.com.atlassian.crowd.directory=INFO
log4j.logger.com.atlassian.confluence.user.crowd=INFO
log4j.logger.com.atlassian.confluence.user.migration=INFO
log4j.logger.com.atlassian.confluence.user.DefaultUserAccessor=INFO
# log4j.logger.com.atlassian.confluence.cache.option.OptionalReadThroughCache=DEBUG
# log4j.logger.com.atlassian.confluence.upgrade.upgradetask.AtlassianUserToEmbeddedCrowdDataUpgradeTask=DEBUG
# log4j.logger.com.atlassian.crowd.embedded.hibernate2.batch.SpringHibernate2SessionFactory=DEBUG
# log4j.logger.com.atlassian.crowd.embedded.propertyset.DebugLoggingPropertySet=DEBUG
# log4j.logger.com.atlassian.crowd.directory.DbCachingRemoteDirectory=DEBUG
# log4j.logger.com.atlassian.crowd.directory.DbCachingRemoteDirectoryCache=DEBUG
# log4j.logger.com.atlassian.crowd.directory.SpringLDAPConnector=DEBUG
# log4j.logger.com.atlassian.crowd.directory.ldap.cache.RemoteDirectoryCacheRefresher=DEBUG
# log4j.logger.com.atlassian.crowd.manager.directory.DirectorySynchroniser=DEBUG

# Log failed login attempts when elevated security check is required - set level to DEBUG to log all failed attempts
log4j.logger.com.atlassian.confluence.security.login.DefaultLoginManager=INFO

# Scheduled Job Admin
#log4j.logger.com.atlassian.confluence.schedule=INFO
#log4j.logger.com.atlassian.confluence.plugins.schedule=INFO

# XHTML migration logging
log4j.logger.com.atlassian.confluence.content.render.xhtml.migration=INFO

# Prevent REST resources from WARNING when they receive a FORM post but use @FormParam to consume the body
log4j.logger.com.sun.jersey.spi.container.servlet.WebComponent=INFO

# EHCache logging
log4j.logger.net.sf.ehcache.pool.impl.DefaultSizeOfEngine=ERROR

log4j.logger.com.atlassian.confluence.cluster.hazelcast.HazelcastClusterSafetyManager=INFO
log4j.logger.com.atlassian.confluence.cluster.hazelcast.monitoring.HazelcastMembershipListener=INFO

# Synchrony logging
log4j.logger.com.atlassian.confluence.plugins.synchrony=INFO
log4j.logger.com.atlassian.confluence.plugins.synchrony.bootstrap.LoggingOutputHandler=DEBUG, synchronylog
log4j.additivity.com.atlassian.confluence.plugins.synchrony.bootstrap.LoggingOutputHandler=false

# Atlassian diagnostics logging
log4j.logger.atlassian-monitor=INFO, diagnostics
log4j.appender.diagnostics=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.diagnostics.LogFileName=atlassian-diagnostics.log
log4j.appender.diagnostics.Threshold=DEBUG
log4j.appender.diagnostics.MaxFileSize=20480KB
log4j.appender.diagnostics.MaxBackupIndex=5
log4j.appender.diagnostics.layout=com.atlassian.confluence.util.PatternLayoutWithContext
log4j.appender.diagnostics.layout.ConversionPattern=[%d] %m%n

# Cloud Migration Assistant Plugin
log4j.logger.com.atlassian.migration.agent=INFO

# Synchrony eviction
log4j.logger.com.atlassian.confluence.upgrade.upgradetask.SynchronyEvictionPropertiesCleanupUpgradeTask=INFO

# Rate limiting
log4j.logger.com.atlassian.ratelimiting=INFO

# Tasks rejected by TreadPoolExecutor
log4j.logger.com.atlassian.confluence.event.MonitorableCallerRunsPolicy=INFO

# mark logs rest api support
# when a new appender is added - if we would like to have the marker functionality for this appender we need to add it to this list
log4j.logger.com.atlassian.confluence.impl.admin.actions.MarkAllLogsAction=INFO, \
  confluencelog, synchronylog, securitylog, indexlog, \
  outgoingmaillog, sqllog, luceneQuery, errorlog, diagnostics
log4j.additivity.com.atlassian.confluence.impl.admin.actions.MarkAllLogsAction=false

#####################################################
# JMX metrics logging
#####################################################

log4j.appender.jmxLogAppender=com.atlassian.confluence.logging.ConfluenceHomeLogAppender
log4j.appender.jmxLogAppender.LogFileName=atlassian-confluence-jmx.log
log4j.appender.jmxLogAppender.Threshold=ALL
log4j.appender.jmxLogAppender.MaxFileSize=20480KB
log4j.appender.jmxLogAppender.MaxBackupIndex=5
log4j.appender.jmxLogAppender.layout=com.atlassian.logging.log4j.NewLineIndentingFilteringPatternLayout
log4j.appender.jmxLogAppender.layout.ConversionPattern=%d %m%n

log4j.logger.jmx-logger=INFO, jmxLogAppender
