#----------------------------------------------------------------------------
# Velocity configuration file. Default values shown commented, changed
# values uncommented. - brett
#----------------------------------------------------------------------------
# This velocity properties file is used by VelocityUtils rendering velocity
# files while things being rendered in atlassian template renderer uses the
# default one.
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# R U N T I M E  L O G
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
#  LogSystem to use: log4j with category "velocity"
#----------------------------------------------------------------------------
# Log4J is configured in log4j.properties.
#----------------------------------------------------------------------------
runtime.log.logsystem.class = org.apache.velocity.runtime.log.SimpleLog4JLogSystem
runtime.log.logsystem.log4j.category=velocity

# prevent velocity from generating the log file.
# runtime.log.logsystem.class = com.atlassian.confluence.util.velocity.NullVelocityLogSystem

#----------------------------------------------------------------------------
# This controls if Runtime.error(), info() and warn() messages include the
# whole stack trace. The last property controls whether invalid references
# are logged.
#----------------------------------------------------------------------------

runtime.log.error.stacktrace = false
runtime.log.warn.stacktrace = false
runtime.log.info.stacktrace = false
#runtime.log.invalid.reference = true

#----------------------------------------------------------------------------
# T E M P L A T E  E N C O D I N G
#----------------------------------------------------------------------------

input.encoding=UTF-8
output.encoding=UTF-8

#----------------------------------------------------------------------------
# F O R E A C H  P R O P E R T I E S
#----------------------------------------------------------------------------
# These properties control how the counter is accessed in the #foreach
# directive. By default the reference $velocityCount will be available
# in the body of the #foreach directive. The default starting value
# for this reference is 1.
#----------------------------------------------------------------------------

#directive.foreach.counter.name = velocityCount
#directive.foreach.counter.initial.value = 1

#----------------------------------------------------------------------------
# I N C L U D E  P R O P E R T I E S
#----------------------------------------------------------------------------
# These are the properties that governed the way #include'd content
# is governed.
#----------------------------------------------------------------------------

#directive.include.output.errormsg.start = <!-- include error :
#directive.include.output.errormsg.end   =  see error log -->

#----------------------------------------------------------------------------
# P A R S E  P R O P E R T I E S
#----------------------------------------------------------------------------

directive.parse.max.depth = 10

#----------------------------------------------------------------------------
# T E M P L A T E  L O A D E R S
#----------------------------------------------------------------------------

# use the webwork file and classpath loaders, as well as a custom classpath loader (defined below)
resource.loader=hibernate,wwfile,confclass,confplugin

# reduce parser memory usage when idle
parser.pool.class=com.atlassian.confluence.velocity.ConfluenceParserPool

# custom classpath loader (for mail templates)
confclass.resource.loader.description=Confluence classpath loader
confclass.resource.loader.class=org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader

# hibernate resource loader (for custom decorators)
hibernate.resource.loader.class=com.atlassian.confluence.setup.velocity.HibernateResourceLoader
hibernate.resource.loader.description=Hibernate loader
hibernate.resource.loader.confluence.velocity13.compatibility=true
hibernate.resource.loader.confluence.space.decorator.loader=true

# debugging resource loader (sending messages to confluence log)
debug.resource.loader.class=com.atlassian.confluence.setup.velocity.DebugResourceLoader
debug.resource.loader.description=Debugging loader

# dynamic plugin classpath loader (for plugin resources)
confplugin.resource.loader.description=Confluence Dynamic Plugin classpath loader
confplugin.resource.loader.class=com.atlassian.confluence.setup.velocity.DynamicPluginResourceLoader
confplugin.resource.loader.confluence.velocity13.compatibility=true

# set caching on for resource loaders (see com.opensymphony.webwork.views.velocity.VelocityManager)
# comment in these lines to add template caching (faster)
wwfile.resource.loader.cache=true
confclass.resource.loader.cache=true
hibernate.resource.loader.cache=true
confplugin.resource.loader.cache=true
file.resource.loader.modificationCheckInterval=-1

#----------------------------------------------------------------------------
# VELOCIMACRO PROPERTIES
#----------------------------------------------------------------------------
# global : name of default global library.  It is expected to be in the regular
# template path.  You may remove it (either the file or this property) if
# you wish with no harm.
#----------------------------------------------------------------------------

velocimacro.library = template/includes/macros-deprecated.vm, template/includes/macros.vm,\
   template/includes/menu-macros.vm,\
   template/includes/auimacros.vm

#velocimacro.permissions.allow.inline = true
velocimacro.permissions.allow.inline.to.replace.global = true
#velocimacro.permissions.allow.inline.local.scope = false

#velocimacro.context.localscope = false
#velocimacro.library.autoreload = true

#----------------------------------------------------------------------------
# INTERPOLATION
#----------------------------------------------------------------------------
# turn off and on interpolation of references and directives in string
# literals.  ON by default :)
#----------------------------------------------------------------------------

#runtime.interpolate.string.literals = true

#----------------------------------------------------------------------------
# RESOURCE MANAGEMENT
#----------------------------------------------------------------------------
# Allows alternative ResourceManager and ResourceCache implementations
# to be plugged in.
#----------------------------------------------------------------------------

resource.manager.class=com.atlassian.confluence.util.velocity.CompatibleVelocityResourceManager

runtime.introspector.uberspect=com.atlassian.confluence.velocity.introspection.ConfluenceAnnotationBoxingUberspect

userdirective=com.opensymphony.webwork.views.velocity.ParamDirective,com.opensymphony.webwork.views.velocity.TagDirective,com.opensymphony.webwork.views.velocity.BodyTagDirective,com.atlassian.confluence.setup.velocity.ApplyDecoratorDirective, com.atlassian.confluence.setup.velocity.ParamDirective, \
com.atlassian.confluence.setup.velocity.RenderVelocityTemplateDirective,com.atlassian.confluence.setup.velocity.TrimDirective, com.atlassian.confluence.setup.velocity.HtmlSafeDirective, com.atlassian.confluence.setup.velocity.SkipLinkDirective, com.atlassian.confluence.setup.velocity.DisableAntiXssDirective, \
com.atlassian.confluence.setup.velocity.ProfilingParseDirective

# runtime.introspector.uberspect=com.atlassian.confluence.util.velocity.debug.UberspectDebugDecorator

# CONF-15389 - recursion limit added in Velocity 1.6. Set to infinity
velocimacro.max.depth=-1

# ----------------------------------------------------------------------------
# SECURE INTROSPECTOR
# ----------------------------------------------------------------------------
# If selected, prohibits methods in certain classes and packages from being
# accessed. If you are adding new package or class restriction, please also
# add it to /confluence-core/confluence/src/etc/java/org/apache/velocity/runtime/defaults/velocity.properties
# The list is maintained at:
# https://extranet.atlassian.com/display/CSPF/Restrict+packages+and+classes+usage+from+velocity+files
# ----------------------------------------------------------------------------

introspector.restrict.packages = java.lang.reflect,\
com.atlassian.cache,\
com.atlassian.confluence.util.http,\
com.atlassian.failurecache,\
com.atlassian.vcache,\
com.atlassian.sal.api.net,\
com.google.common.cache,\
com.google.common.net,\
com.hazelcast,java.jms,\
java.rmi,\
javax.management,\
javax.naming,\
org.apache.catalina.session,\
org.apache.commons.httpclient,\
org.apache.httpcomponents.httpclient,\
org.apache.http.client,\
org.ehcache,\
com.google.common.reflect,\
com.sun.jmx,com.sun.jna,\
javax.xml,jdk.nashorn,\
net.bytebuddy,\
net.sf.cglib,org.apache.bcel,\
org.javassist,org.ow2.asm,\
sun.awt.shell,\
sun.corba,\
sun.invoke,\
sun.launcher,\
sun.management,\
sun.misc,\
sun.net,\
sun.nio,\
sun.print,\
sun.reflect,\
sun.rmi,\
sun.security,\
sun.tracing,\
sun.tools.jar,\
com.atlassian.activeobjects,\
com.atlassian.hibernate,\
java.sql,\
javax.persistence,\
javax.sql,\
liquibase,\
net.java.ao,\
net.sf.hibernate,\
com.atlassian.confluence.setup.bandana,\
com.atlassian.filestore,\
com.atlassian.media,\
com.google.common.io,java.io,\
java.nio,java.util.jar,\
java.util.zip,\
org.apache.commons.io,\
com.atlassian.confluence.impl.util.sandbox,\
com.atlassian.confluence.util.io,\
com.atlassian.confluence.util.sandbox,\
com.atlassian.quartz,\
com.atlassian.scheduler,\
com.atlassian.utils.process,\
com.atlassian.util.concurrent,\
io.atlassian.util.concurrent,\
java.util.concurrent,\
org.apache.commons.exec,\
org.springframework.expression.spel,\
org.springframework.util.concurrent,\
org.quartz,\
oshi

introspector.restrict.classes = java.lang.Class,\
java.lang.ClassLoader,\
java.lang.Compiler,\
java.lang.InheritableThreadLocal,\
java.lang.Package,\
java.lang.Process,\
java.lang.Runtime,\
java.lang.RuntimePermission,\
java.lang.SecurityManager,\
java.lang.System,\
java.lang.Thread,\
java.lang.ThreadGroup,\
java.lang.ThreadLocal,\
javax.script.ScriptEngineManager,\
javax.servlet.ServletContext,\
javax.persistence.EntityManager,\
org.apache.tomcat.InstanceManager,\
org.springframework.context.ApplicationContext,\
com.atlassian.applinks.api.ApplicationLinkRequestFactory,\
com.atlassian.core.util.ClassLoaderUtils,\
com.atlassian.core.util.ClassHelper
introspector.allowlist.classes = java.io.Serializable,\
java.lang.reflect.Proxy,\
net.sf.hibernate.proxy.HibernateProxy,\
net.sf.cglib.proxy.Factory,\
java.io.ObjectInputValidation,\
net.java.ao.Entity,\
net.java.ao.RawEntity,\
net.java.ao.EntityProxyAccessor
