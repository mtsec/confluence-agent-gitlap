#* @vtlvariable name="action" type="com.atlassian.confluence.fixonly.actions.FixLicenseAction" *#
<html>
<head>
    <title>$action.getActionName($action.getClass().getName())</title>
</head>
<body>

<style type="text/css">
    
    .errorBox {
        background-color: #fcc;
        border: 1px solid #c00;
        padding: 12px;
    }

    .errorMessage {
        color: red
    }

    .success {
        color: green
    }

    table td.label {
        background-color: #f0f0f0;
        vertical-align: top;
        font-weight: bold;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    table td, table th {
        padding: 10px;
        vertical-align: top;
        line-height: 16pt;
        border-top: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
    }

    input {
        font-size: 11pt;
    }

</style>

<content tag="heading">$action.getText("update.confluence.license")</content>

#parse ("/template/includes/actionerrors.vm")

#if ($action.licenseSubmitted == true && $action.fieldErrors.isEmpty() && $action.actionErrors.isEmpty())

    <p class="success">$action.getText('license.update.success')</p>
    <p>$action.getText('license.update.success.restart.note')</p>
#else

    #if ($action.licenseReadable)
        <p>$action.getText("license.update.instructions")</p>
    #else
        <p>$action.getText("no.valid.license")</p>
        <p>$action.getText("enter.key.below", [$action.getText('url.atlassian.confluence.generate.eval.license', [$generalUtil.versionNumber, $generalUtil.buildNumber, $action.sid])])</p>
    #end

    #set($supportPeriodPlaceholder = "license.supportperiod.server")
    #set($supportEndedPlaceholder = "confluence.support.has.ended.server")
    #set($supportEndsPlaceholder = "support.ends.on.server")
    #set($buyRenewalPlaceholder = "buy.renewal.server")
    #if($action.confluenceLicense.subscription)
        #set($supportPeriodPlaceholder = "license.supportperiod.datacenter")
        #set($supportEndedPlaceholder = "confluence.support.has.ended.datacenter")
        #set($supportEndsPlaceholder = "support.ends.on.datacenter")
        #set($buyRenewalPlaceholder = "buy.renewal.datacenter")
    #end

    <table>
        #if ($action.licenseReadable)
        <tr #if($action.confluenceLicense.expired || $action.hasSupportPeriodExpired)bgcolor="ffcccc"#else bgcolor="f0f0f0"#end">
            #if ($action.evaluationLicense)
                <td class="label">
                    $action.getText("license.expiration")
                </td>
                #if ($action.conflueceLicense.expired)
                    <td bgcolor="#ffcccc">
                        $action.getText("evaluation.expired", [$action.confluenceLicense.expiryDate])<br/>
                        <strong>$action.getText("contact.atlassian.to.purchase", [$action.getText("mailto.sales")])</strong>
                    </td>
                #else
                    <td bgcolor="#ffffff">
                        $action.getText("evaluation.expires.in", [$expiryTime])
                    </td>
                #end
            #else
                <td class="label">
                    $action.getText($supportPeriodPlaceholder)
                </td>
                <td #if($action.hasSupportPeriodExpired)bgcolor="ffcccc" #else bgcolor="ffffff" #end>
                    #if ($action.hasSupportPeriodExpired)
                        $action.getText($supportEndedPlaceholder,["$action.supportPeriodEnd"])
                        <p>$action.getText($buyRenewalPlaceholder, ["<a href='$action.getText('url.atlassian.license.renew')'> ", "</a>", "<a href='$action.getText('url.atlassian.license.help')'> ", "</a>"])
                        <p>$action.getText("license.editing.help", ["<a href='$docBean.getLink('help.managing.confluence.license')'> ", "</a>"])
                    #else
                        $action.getText($supportEndsPlaceholder, [$action.supportPeriodEnd]) $action.getText("license.editing.help", ["<a href='$docBean.getLink('help.managing.confluence.license')'> ", "</a>"])
                    #end
                </td>
            #end
        </tr>
        #end
        <form method="POST" action="dofixupdatelicense.action" name="updateLicenseForm">
            <tr>
                <td class="label">
                    $action.getText("license.name")
                </td>
                <td>
                    #fielderror('licenseString')
                    #tag("Textarea" "label='license.name'" "name='licenseString'" "rows=10" "cols=85" "theme='simple'")<br/>
                    <br/>
                    #tag("Submit" "name='update'" "value='update.name'" "theme='notable'" )
            </tr>
        </form>
    </table>

#end

</body>
</html>