<%@ page import="com.atlassian.confluence.impl.health.web.JohnsonPageEndpointsProvider,
                 com.atlassian.confluence.impl.health.web.JohnsonPageI18NProvider,
                 com.atlassian.config.util.BootstrapUtils"%>
<html lang="en">
<head>
    <base href="<%=request.getContextPath() + "/"%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Errors</title>
    <link rel="shortcut icon" href="johnson/fav-confluence.ico">
    <%
        JohnsonPageI18NProvider johnsonPageI18NProvider = (JohnsonPageI18NProvider) BootstrapUtils.getBootstrapContext().getBean("johnsonPageI18NProvider");
        String translations = johnsonPageI18NProvider.getTranslations().serialize();
    %>
    <script>
        window.i18n = <%=translations%>;
        window.endpoints = <%=JohnsonPageEndpointsProvider.getEndpoints(request.getContextPath()).serialize()%>;
    </script>
</head>
<%-- To recompile Johnson app please run "npm run build-johnson" from root folder --%>
<%-- Source code of Johnson is located at confluence-frontend/johnson-page --%>
<%@ include file="./johnson/johnson-error-page.html" %>
</html>
